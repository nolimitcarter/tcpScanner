# tcpScanner

Simple TCP scanners written in Go

Project to further my knowledge in TCP handshakes, testing for port availability, performing both concurrent and non-concurrent scanning, multi-channel communication, and eventually more.
