package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func shownmap(name string) { //defining shownmap function
	url := "https://api.hackertarget.com/nmap/?q=" + name
	resp, err := http.Get(url)
	// handle the error if there is one
	if err != nil {
		panic(err)
	}
	// do this now so it won't be forgotten
	defer resp.Body.Close()
	// reads html as a slice of bytes
	html, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	// show the nmap as a string %s
	fmt.Printf("%s\n", html)
}
